var next; // next fade()
var nextToast; // next toast

module.exports.fade = function (obj) {
  obj.hide();

  var done = function() {
    obj.fadeIn(100, function() {
      if (next()) {
        next = null;
      }
    });
  }

  if (!next) {
    next = function() {
      return true;
    }
    done();
  } else {
    var prev = next;
    next = function() {
      if (!prev) {
        return true;
      }
      if (prev()) {
        prev = null;
        done();
      }
      return false;
    }
  }
}

module.exports.toast = function (msg) {
  var padding = 20;
  var last = $(".toast").last();
  var top = padding;
  if (last.length) {
    top = last.position().top + last.height() + padding;
  }
	var obj = $("<div class='toast ui-loader ui-overlay-shadow ui-body-e ui-corner-all'><h3>" + msg + "</h3></div>")
	.css({ display: "block", 
		opacity: 1,
    background: "#fff",
		position: "fixed",
		padding: "7px",
		"text-align": "center",
		width: "270px",
		left: ($(window).width() - 284) - padding,
		top: top})
	.appendTo($.mobile.pageContainer);

  var done = function() {
    obj.delay(1500);
  	obj.fadeOut(400, function() {
      var h = $(this).height() + padding;
  		$(this).remove();
      $(".toast").animate({
        top: "-=" + h
      }, 400);
      if (nextToast()) {
        nextToast = null;
      }
  	});
  }

  if (!nextToast) {
    nextToast = function() {
      return true;
    }
    done();
  } else {
    var prev = nextToast;
    nextToast = function() {
      if (!prev) {
        return true;
      }
      if (prev()) {
        prev = null;
        done();
      }
      return false;
    }
  }
}
