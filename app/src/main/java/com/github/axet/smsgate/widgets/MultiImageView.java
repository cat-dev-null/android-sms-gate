package com.github.axet.smsgate.widgets;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.github.axet.androidlibrary.widgets.ThemeUtils;

import java.util.ArrayList;


public class MultiImageView extends FrameLayout {
    ArrayList<Bitmap> list = new ArrayList<>();
    AccelerateDecelerateInterpolator pol = new AccelerateDecelerateInterpolator();
    int pad;

    public MultiImageView(Context context) {
        super(context);

        create();
    }

    public MultiImageView(Context context, AttributeSet attrs) {
        super(context, attrs);

        create();
    }

    public MultiImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        create();
    }

    @TargetApi(21)
    public MultiImageView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);

        create();
    }

    void create() {
        setDrawingCacheEnabled(true);
        pad = ThemeUtils.dp2px(getContext(), 30);
    }

    public void add(Uri uri) {
        FrameLayout f = new FrameLayout(getContext());
        ImageView image = new ImageView(getContext());
        image.setImageURI(uri);
        image.setMaxHeight(ThemeUtils.dp2px(getContext(), 100));
        image.setScaleType(ImageView.ScaleType.CENTER_CROP);
        f.addView(image);
        addView(f);
        requestLayout();
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);

        if (getChildCount() == 0) {
            return;
        }

        left = getPaddingLeft();
        top = getPaddingTop();
        right = getWidth() - getPaddingRight();
        bottom = getHeight() - getPaddingBottom();

        if (getChildCount() == 1) {
            View v = getChildAt(0);
            v.layout(left, top, Math.min(left + v.getWidth(), right), Math.min(top + v.getHeight(), bottom));
            return;
        }

        for (int i = 0; i < getChildCount(); i++) {
            float p = pol.getInterpolation(i / (getChildCount() - 1f));
            View v = getChildAt(i);
            int o = (int) (pad * p); // offset
            int ao = pad - o; // anti offset
            int l = left + o;
            int t = top + o;
            int r = Math.min(l + v.getWidth(), right - ao);
            int b = Math.min(t + v.getHeight(), bottom - ao);
            v.layout(l, t, r, b);
        }
    }

    public Bitmap getPreview() {
        buildDrawingCache();
        return getDrawingCache();
    }
}
