package com.github.axet.smsgate.services;

import static com.zegoggles.smssync.service.BackupType.REGULAR;
import static com.zegoggles.smssync.service.BackupType.UNKNOWN;

import android.app.Notification;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.util.Log;

import com.github.axet.androidlibrary.app.AlarmManager;
import com.github.axet.androidlibrary.app.NotificationManagerCompat;
import com.github.axet.androidlibrary.preferences.OptimizationPreferenceCompat;
import com.github.axet.androidlibrary.widgets.NotificationChannelCompat;
import com.github.axet.smsgate.R;
import com.github.axet.smsgate.app.Storage;
import com.zegoggles.smssync.App;
import com.zegoggles.smssync.mail.DataType;
import com.zegoggles.smssync.preferences.Preferences;
import com.zegoggles.smssync.service.Alarms;
import com.zegoggles.smssync.service.BackupType;
import com.zegoggles.smssync.service.state.BackupState;
import com.zegoggles.smssync.service.state.SmsSyncState;

import java.util.ArrayList;

public class FileSmsService extends Service {
    public static final String TAG = FileSmsService.class.getSimpleName();

    public static int NOTIFICATION_ICON = 303;

    OptimizationPreferenceCompat.NotificationIcon icon;
    Storage storage;
    Thread thread;
    Handler handler = new Handler();
    ArrayList<Runnable> dones = new ArrayList<>();

    public static void start(Context context, Intent intent) {
        OptimizationPreferenceCompat.startService(context, intent);
        scheduleSmsBackup(context);
    }

    public static void startIfEnabled(Context context) {
        if (!Storage.isEnabled(context))
            return;
        incoming(context, 0);
    }

    public static void stop(Context context) {
        Intent intent = new Intent(context, FileSmsService.class);
        context.stopService(intent);
        Alarms a = new Alarms(context);
        AlarmManager.cancel(context, a.createPendingIntent(context, UNKNOWN, FileSmsService.class));
    }

    public static long scheduleSmsBackup(Context context) {
        Alarms a = new Alarms(context);
        return a.scheduleBackup(new Preferences(context).getRegularTimeoutSecs(), REGULAR, false, FileSmsService.class);
    }

    public static void incoming(Context context, long last) {
        if (!Storage.isEnabled(context))
            return;
        Intent intent = new Intent(context, FileSmsService.class).putExtra("last", last);
        start(context, intent);
    }

    public static void incoming(Context context, boolean skip) {
        if (Storage.getLastSyncDate(context) == 0 && skip)
            Storage.setLastSyncDate(context, System.currentTimeMillis());
        incoming(context, 0);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.v(TAG, "FileSmsService create");

        icon = new OptimizationPreferenceCompat.NotificationIcon(this, NOTIFICATION_ICON) {
            @Override
            public void updateIcon() {
                updateIcon(null); // we do not need two icons on low API phones
            }

            @Override
            public boolean isOptimization() {
                return Build.VERSION.SDK_INT >= 26 && context.getApplicationInfo().targetSdkVersion >= 26; // show double icons for API26+
            }

            @Override
            public Notification build(Intent intent) {
                return new OptimizationPreferenceCompat.PersistentIconBuilder(context).setWhen(notification)
                        .create(R.style.AppThemeDark, new NotificationChannelCompat(context, "status", "FileSmsService", NotificationManagerCompat.IMPORTANCE_LOW))
                        .setText("FileSmsService")
                        .setAdaptiveIcon(R.drawable.ic_launcher_foreground)
                        .setSmallIcon(R.drawable.ic_launcher_notification).build();
            }
        };
        icon.create();

        storage = new Storage(this);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        scheduleSmsBackup(this);
        long last = 0;
        if (intent != null)
            last = intent.getLongExtra("last", 0);
        incoming(last);
        return super.onStartCommand(intent, flags, startId);
    }

    public void incoming(final long last) {
        final Runnable messages = new Runnable() {
            int count = 0;

            @Override
            public void run() {
                final Runnable that = this;
                Runnable done = new Runnable() {
                    @Override
                    public void run() {
                        dones.remove(that);
                        if (dones.isEmpty())
                            stop(FileSmsService.this);
                    }
                };
                count++;
                if (count > 10) {
                    handler.post(done);
                    return;
                }
                Storage storage = new Storage(FileSmsService.this);
                if (storage.messages() < last) {
                    handler.postDelayed(this, 1000);
                } else {
                    handler.post(done);
                }
            }
        };
        dones.add(messages);
        if (thread == null) {
            thread = new Thread("Sms Thread") {
                @Override
                public void run() {
                    Looper.prepare();
                    try {
                        messages.run();
                    } finally {
                        thread = null;
                    }
                }
            };
            thread.start();
        } else {
            handler.postDelayed(messages, 1000);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (thread != null) {
            thread.interrupt();
            try {
                thread.join();
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
            thread = null;
        }
        icon.close();
        handler.removeCallbacksAndMessages(null);
        App.bus.post(new BackupState(SmsSyncState.INITIAL, 0, 0, BackupType.MANUAL, DataType.SMS, null));
    }
}
