package com.github.axet.smsgate.services;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.provider.Telephony;
import android.util.Log;

import com.fsck.k9.mail.AuthenticationFailedException;
import com.fsck.k9.mail.Body;
import com.fsck.k9.mail.BodyPart;
import com.fsck.k9.mail.FetchProfile;
import com.fsck.k9.mail.Flag;
import com.fsck.k9.mail.Folder;
import com.fsck.k9.mail.MessagingException;
import com.fsck.k9.mail.internet.MimeMultipart;
import com.fsck.k9.mail.internet.MimeUtility;
import com.fsck.k9.mail.store.imap.ImapMessage;
import com.fsck.k9.mail.store.imap.XOAuth2AuthenticationFailedException;
import com.github.axet.smsgate.app.SMSApplication;
import com.github.axet.smsgate.app.SmsStorage;
import com.github.axet.smsgate.providers.SMS;
import com.squareup.otto.Subscribe;
import com.zegoggles.smssync.App;
import com.zegoggles.smssync.auth.TokenRefreshException;
import com.zegoggles.smssync.auth.TokenRefresher;
import com.zegoggles.smssync.mail.BackupImapStore;
import com.zegoggles.smssync.mail.DataType;
import com.zegoggles.smssync.mail.Headers;
import com.zegoggles.smssync.preferences.Preferences;
import com.zegoggles.smssync.service.BackupConfig;
import com.zegoggles.smssync.service.UserCanceled;
import com.zegoggles.smssync.service.state.ReplyState;
import com.zegoggles.smssync.service.state.SmsSyncState;

import org.apache.commons.io.IOUtils;
import org.jetbrains.annotations.NotNull;
import org.jsoup.Jsoup;
import org.jsoup.internal.StringUtil;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.NodeTraversor;
import org.jsoup.select.NodeVisitor;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.zegoggles.smssync.App.TAG;
import static com.zegoggles.smssync.service.state.SmsSyncState.CALC;
import static com.zegoggles.smssync.service.state.SmsSyncState.CANCELED_RESTORE;
import static com.zegoggles.smssync.service.state.SmsSyncState.ERROR;
import static com.zegoggles.smssync.service.state.SmsSyncState.FINISHED_REPLYSMS;
import static com.zegoggles.smssync.service.state.SmsSyncState.LOGIN;
import static com.zegoggles.smssync.service.state.SmsSyncState.REPLYSMS;
import static com.zegoggles.smssync.service.state.SmsSyncState.UPDATING_THREADS;

public class ImapSmsReplyTask extends AsyncTask<BackupConfig, ReplyState, ReplyState> {
    private Set<String> smsIds = new HashSet<>();
    private Set<String> uids = new HashSet<>();

    Context context;
    private final ImapSmsReplyService service;
    private final TokenRefresher tokenRefresher;
    Preferences preferences;

    public static String extractMessage(String body) {
        String msg = body;

        // drop quotes. lines starting with: ">...."
        {
            Pattern p = Pattern.compile("^>.*$", Pattern.MULTILINE);
            Matcher m = p.matcher(msg);
            msg = m.replaceAll("");
        }

        // drop quotes. lines starting with: "<...."
        {
            Pattern p = Pattern.compile("^<.*$", Pattern.MULTILINE);
            Matcher m = p.matcher(msg);
            msg = m.replaceAll("");
        }

        // drop multi empty lines
        {
            Pattern p = Pattern.compile("^\\s*$\\n", Pattern.MULTILINE);
            Matcher m = p.matcher(msg);
            msg = m.replaceAll("");
        }

        {
            Pattern p = Pattern.compile("^\\s*", Pattern.MULTILINE);
            Matcher m = p.matcher(msg);
            msg = m.replaceAll("");
        }

        {
            Pattern p = Pattern.compile("\\s*$", Pattern.MULTILINE);
            Matcher m = p.matcher(msg);
            msg = m.replaceAll("");
        }

        msg = msg.trim();

        return msg;
    }

    public static class RefMap {
        // message id
        public String id;
        // incoming SMS phone number
        public String phone;
        // incomming SMS date
        public long date;
        // thread id
        public String thread;
        // groupped emails (replies)
        public List<ImapMessage> group = new ArrayList<ImapMessage>();
        // sim id
        public int simId = -1;

        public RefMap() {
        }

        public RefMap(ImapMessage m) throws MessagingException {
            id = m.getMessageId();

            if (this.phone == null) { // this refmap group has a key message with phone number
                String[] addrs = m.getHeader(Headers.ADDRESS);
                if (addrs.length > 0) {
                    this.phone = addrs[0];
                }
            }

            if (this.date == 0) {
                String[] dates = m.getHeader(Headers.DATE);
                if (dates.length > 0) {
                    this.date = Long.valueOf(dates[0]);
                }
            }

            if (this.thread == null) {
                String[] thread = m.getHeader(Headers.THREAD_ID);
                if (thread.length > 0) {
                    this.thread = thread[0];
                }
            }

            if (this.simId == -1) {
                String[] ss = m.getHeader(Headers.SIM_ID);
                if (ss.length > 0) {
                    this.simId = Integer.parseInt(ss[0]);
                }
            }
        }

        public boolean isKey() {
            return phone != null;
        }

        public void apply(RefMap m) throws MessagingException {
            if (this.id == null) {
                this.id = m.id;
            }

            if (this.phone == null) {
                this.phone = m.phone;
            }

            if (this.date == 0) {
                this.date = m.date;
            }

            if (this.thread == null) {
                this.thread = m.thread;
            }

            if (this.simId == -1) {
                this.simId = m.simId;
            }
        }

        public void add(ImapMessage m) throws MessagingException {
            group.add(m);
        }
    }

    public static class FormattingVisitor implements NodeVisitor {
        private static final int maxWidth = 80;
        private int width = 0;
        private StringBuilder accum = new StringBuilder(); // holds the accumulated text

        // hit when the node is first seen
        public void head(Node node, int depth) {
            String name = node.nodeName();
            if (node instanceof TextNode)
                append(((TextNode) node).text()); // TextNodes carry all user-readable text in the DOM.
            else if (name.equals("li"))
                append("\n * ");
            else if (name.equals("dt"))
                append("  ");
            else if (StringUtil.in(name, "p", "h1", "h2", "h3", "h4", "h5", "tr", "div"))
                append("\n");
        }

        // hit when all of the node's children (if any) have been visited
        public void tail(Node node, int depth) {
            String name = node.nodeName();
            if (StringUtil.in(name, "br", "dd", "dt", "p", "h1", "h2", "h3", "h4", "h5"))
                append("\n");
            else if (name.equals("a"))
                append(String.format(" <%s>", node.absUrl("href")));
        }

        // appends text to the string builder with a simple word wrap method
        private void append(String text) {
            if (text.startsWith("\n"))
                width = 0; // reset counter if starts with a newline. only from formats above, not in natural text
            if (text.equals(" ") &&
                    (accum.length() == 0 || StringUtil.in(accum.substring(accum.length() - 1), " ", "\n")))
                return; // don't accumulate long runs of empty spaces

            if (text.length() + width > maxWidth) { // won't fit, needs to wrap
                String words[] = text.split("\\s+");
                for (int i = 0; i < words.length; i++) {
                    String word = words[i];
                    boolean last = i == words.length - 1;
                    if (!last) // insert a space if not the last word
                        word = word + " ";
                    if (word.length() + width > maxWidth) { // wrap and reset counter
                        accum.append("\n").append(word);
                        width = word.length();
                    } else {
                        accum.append(word);
                        width += word.length();
                    }
                }
            } else { // fits as is, without need to wrap text
                accum.append(text);
                width += text.length();
            }
        }

        @Override
        public String toString() {
            return accum.toString();
        }
    }

    public ImapSmsReplyTask(ImapSmsReplyService service,
                            Preferences preferences,
                            TokenRefresher tokenRefresher) {
        this.context = service;
        this.service = service;
        this.preferences = preferences;
        this.tokenRefresher = tokenRefresher;
    }

    @Override
    protected void onPreExecute() {
        App.bus.register(this);
    }

    @Subscribe
    public void userCanceled(UserCanceled canceled) {
        cancel(false);
    }

    @NotNull
    protected ReplyState doInBackground(BackupConfig... params) {
        if (params == null || params.length == 0)
            throw new IllegalArgumentException("No config passed");
        BackupConfig config = params[0];
        try {
            service.acquireLocks(TAG);
            return reply(config);
        } finally {
            service.releaseLocks();
        }
    }

    String decodeBody(Body body) throws MessagingException, IOException {
        InputStream is = null;
        if (body instanceof MimeMultipart) {
            for (BodyPart bp : ((MimeMultipart) body).getBodyParts()) {
                if (bp.getContentType().contains("text/html")) {
                    is = MimeUtility.decodeBody(bp.getBody());
                    Document doc = Jsoup.parse(IOUtils.toString(is));
                    for (Element element : doc.select("blockquote")) {
                        element.remove();
                    }
                    for (Element element : doc.select(".gmail_quote")) {
                        element.remove();
                    }
                    FormattingVisitor formatter = new FormattingVisitor();
                    NodeTraversor.traverse(formatter, doc);
                    return formatter.toString();
                }
            }
            if (is == null) {
                for (BodyPart bp : ((MimeMultipart) body).getBodyParts()) {
                    if (bp.getContentType().contains("text/plain")) {
                        is = bp.getBody().getInputStream();
                    }
                }
            }
        } else {
            is = MimeUtility.decodeBody(body);
        }
        if (is == null) {
            throw new MessagingException("body.getInputStream() is null for ");
        }
        return IOUtils.toString(is);
    }

    void deleteMessage(BackupImapStore imap, String id) throws MessagingException {
        for (Folder fn : imap.getPersonalNamespaces(false)) {
            BackupImapStore.BackupFolder f = imap.createAndOpenFolder(fn.getName());
            ImapMessage msg = f.getMessagesId(id);
            if (msg != null) {
                msg.setFlag(Flag.DELETED, true);
                f.expunge(msg.getUid());
            }
            f.close();
        }
    }

    private ReplyState reply(BackupConfig config) {
        final BackupImapStore imapStore = config.imapStore;

        int currentRestoredItem = 0;
        try {
            publishProgress(LOGIN);
            imapStore.checkSettings();

//            K9MailLib.setDebug(true);
//            K9MailLib.setDebugSensitive(true);
//            K9MailLib.DEBUG_PROTOCOL_IMAP = true;

            publishProgress(CALC);

            for (String f : ImapSmsReplyService.getSubscribedFolders(context)) {
                final BackupImapStore.BackupFolder folder = imapStore.getCachedFolder(f);
                currentRestoredItem += check(folder, config);
            }

            return new ReplyState(FINISHED_REPLYSMS, 0, 0, config.backupType, null, null);
        } catch (XOAuth2AuthenticationFailedException e) {
            return handleAuthError(config, currentRestoredItem, e);
        } catch (AuthenticationFailedException e) {
            Log.w(TAG, e);
            return transition(ERROR, e);
        } catch (IOException e) {
            Log.e(TAG, "error", e);
            return transition(ERROR, e);
        } catch (MessagingException e) {
            Log.e(TAG, "error", e);
            return transition(ERROR, e);
        } catch (IllegalStateException e) {
            Log.w(TAG, e);
            // usually memory problems (Couldn't init cursor window)
            return transition(ERROR, e);
        } finally {
            imapStore.closeFolders();
        }
    }

    public int check(BackupImapStore.BackupFolder folder, BackupConfig config) throws IOException, MessagingException {
        int currentRestoredItem = 0;

        long now = System.currentTimeMillis();
        SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(context);
        long since = shared.getLong(SMSApplication.PREF_REPLY, 0);
        Date s = null;
        if (since != 0)
            s = new Date(since);
        List<ImapMessage> msgs = folder.getMessages("UID SEARCH HEADER In-Reply-To \"@sms-backup-plus.local\"", s);

        final int itemsToRestoreCount = msgs.size();

        Map<String, RefMap> map = new TreeMap<>();

        if (itemsToRestoreCount > 0) {
            for (; currentRestoredItem < itemsToRestoreCount && !isCancelled(); currentRestoredItem++) {
                ImapMessage m = msgs.get(currentRestoredItem);

                FetchProfile fp = new FetchProfile();
                fp.add(FetchProfile.Item.BODY);
                fp.add(FetchProfile.Item.FLAGS);

                folder.fetch(Arrays.asList(m), fp, null);

                RefMap rm = new RefMap(m);
                if (rm.isKey()) {
                    RefMap rm2 = map.get(rm.id);
                    if (rm2 == null)
                        map.put(rm.id, rm);
                    else
                        rm2.apply(rm);
                } else {
                    String[] refs = m.getHeader("References");
                    for (String rr : refs) {
                        for (String r : rr.split("\n")) {
                            r = r.trim();
                            RefMap rm2 = map.get(r);
                            if (rm2 == null) {
                                rm2 = new RefMap();
                                map.put(r, rm2);
                            }
                            rm2.add(m);
                        }
                    }
                }

                publishProgress(new ReplyState(REPLYSMS, currentRestoredItem + 1, itemsToRestoreCount, config.backupType, null, null));
            }

            for (String k : map.keySet()) {
                RefMap r = map.get(k);
                ImapMessage m = folder.getMessagesId(k);
                if (m != null) {
                    FetchProfile fp = new FetchProfile();
                    fp.add(FetchProfile.Item.BODY);
                    fp.add(FetchProfile.Item.FLAGS);
                    folder.fetch(Arrays.asList(m), fp, null);
                    RefMap rm = new RefMap(m);
                    r.apply(rm);
                }
            }

            if (!isCancelled()) {
                for (String key : map.keySet()) {
                    currentRestoredItem++;

                    RefMap m = map.get(key);

                    if (!m.isKey())
                        continue;

                    SmsStorage storage = new SmsStorage(context);
                    for (ImapMessage im : m.group) {
                        if (im.getHeader(Headers.ADDRESS).length > 0) // skip key (incoming) message
                            continue;
                        if (im.getFlags().contains(Flag.DELETED)) // mis expunge folder may have deleted messages
                            continue;
                        if (im.getFlags().contains(Flag.FORWARDED)) // already sent
                            continue;

                        Date date = im.getSentDate();
                        Body body = im.getBody();
                        String b = decodeBody(body);
                        String msg = extractMessage(b);
                        int simID = m.simId;

                        if (msg != null && !msg.equals("")) {
                            // do not work with sms, not belong to current phone.
                            // here may be two phones with SMS-GATE applications.
                            // each phone must work with it own messages to keep phone number.
                            if (storage.exists(m.date, m.phone, Telephony.Sms.DATE)) { // incoming exists
                                if (!storage.exists(date.getTime(), m.phone, Telephony.Sms.DATE)) { // outgoing does not exists
                                    if (simID != -1)
                                        SMS.send(service, simID, m.phone, msg, date); // m.thread);
                                    else
                                        SMS.send(service, m.phone, msg, date); // m.thread);
                                } else {
                                    Log.v(TAG, "skiping sms send, becase it already sent " + m.date + " " + m.phone);
                                }
                                im.setFlag(Flag.FORWARDED, true);
                                im.setFlag(Flag.SEEN, true);
                            } else {
                                Log.v(TAG, "skiping sms send, becase it not in INBOX " + m.date + " " + m.phone);
                            }
                        }
                    }

                    publishProgress(new ReplyState(REPLYSMS, currentRestoredItem, map.size(), config.backupType, null, null));
                }

//                    for (Folder fn : imapStore.getPersonalNamespaces(false)) {
//                        BackupImapStore.BackupFolder f = imapStore.createAndOpenFolder(fn.getName());
//                        f.expungeSimple();
//                        f.close();
//                    }
            }

            if (!isCancelled()) {
                publishProgress(UPDATING_THREADS);
                SmsStorage storage = new SmsStorage(context);
                storage.updateAllThreads();
            }

            shared.edit().putLong(SMSApplication.PREF_REPLY, now).commit();
        } else {
            Log.d(TAG, "nothing to restore");
        }

        return currentRestoredItem;
    }

    private ReplyState handleAuthError(BackupConfig config, int currentRestoredItem, XOAuth2AuthenticationFailedException e) {
        if (e.getStatus() == 400) {
            Log.d(TAG, "need to perform xoauth2 token refresh", e);
            try {
                tokenRefresher.refreshOAuth2Token();
                // we got a new token, let's retry one more time - we need to pass in a new store object
                // since the auth params on it are immutable
                return reply(config.retryWithStore(service.getBackupImapStore()));
            } catch (MessagingException e1) {
                Log.w(TAG, e1);
            } catch (TokenRefreshException refreshException) {
                Log.w(TAG, refreshException);
            }
        } else {
            Log.w(TAG, "unexpected xoauth status code " + e.getStatus(), e);
        }
        return transition(ERROR, e);
    }

    private void publishProgress(SmsSyncState smsSyncState) {
        publishProgress(smsSyncState, null);
    }

    private void publishProgress(SmsSyncState smsSyncState, Exception exception) {
        publishProgress(transition(smsSyncState, exception));
    }

    private ReplyState transition(SmsSyncState smsSyncState, Exception exception) {
        return service.getState().transition(smsSyncState, exception);
    }

    @Override
    protected void onPostExecute(ReplyState result) {
        if (result != null) {
            Log.d(TAG, "finished (" + result + "/" + uids.size() + ")");
            post(result);
        }
        App.bus.unregister(this);
    }

    @Override
    protected void onCancelled() {
        Log.d(TAG, "restore canceled by user");
        post(transition(CANCELED_RESTORE, null));
        App.bus.unregister(this);
    }

    @Override
    protected void onProgressUpdate(ReplyState... progress) {
        if (progress != null && progress.length > 0 && !isCancelled()) {
            post(progress[0]);
        }
    }

    private void post(ReplyState changed) {
        if (changed == null) return;
        App.bus.post(changed);
    }

    protected Set<String> getSmsIds() {
        return smsIds;
    }
}
