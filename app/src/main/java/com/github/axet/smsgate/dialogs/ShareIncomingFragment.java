package com.github.axet.smsgate.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.github.axet.androidlibrary.crypto.Bitcoin;
import com.github.axet.androidlibrary.services.FileProvider;
import com.github.axet.smsgate.R;
import com.github.axet.smsgate.app.SMSApplication;

import org.apache.commons.io.output.ByteArrayOutputStream;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static org.apache.commons.io.IOUtils.EOF;

public class ShareIncomingFragment extends DialogFragment {
    private static final String TAG = ShareIncomingFragment.class.getSimpleName();
    View v;

    LinearLayout list;
    AlertDialog d;

    String title;
    TextView text;

    Handler handler = new Handler();

    Map<String, ProgressBar> bars = new HashMap<>();

    private class DownloadImageTask extends AsyncTask<ArrayList<Bundle>, Void, Intent> {
        Context context;

        public DownloadImageTask() {
            context = getActivity();
        }

        protected Intent doInBackground(ArrayList<Bundle>... urls) {
            ArrayList<Bundle> uris = urls[0];
            Bitcoin key = SMSApplication.from(getActivity()).keys;
            Intent intent = new Intent();

            ArrayList<Uri> uu = new ArrayList<>();
            try {
                for (Bundle uri : uris) {
                    final String u = uri.getString("uri");
                    String type = uri.getString("mimetype");
                    final String fn = uri.getString("filename");
                    final Long len = new Long(uri.get("filesize").toString());
                    final ProgressBar bar = bars.get(u);
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            bar.setProgress(0);
                            bar.setVisibility(View.VISIBLE);
                        }
                    });
                    InputStream in = new java.net.URL(u).openStream();

                    final ByteArrayOutputStream output = new ByteArrayOutputStream();
                    byte[] buffer = new byte[16 * 1024];
                    int n;
                    long count = 0;
                    while (EOF != (n = in.read(buffer))) {
                        output.write(buffer, 0, n);
                        count += n;
                        final int p = (int) (count * 100 / len);
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                bar.setProgress(p);
                            }
                        });
                    }
                    byte[] bb = output.toByteArray();

                    byte[] buf = key.decrypt(bb);
                    Uri s = SMSApplication.shareFile(context, type, fn, buf);
                    uu.add(s);
                }
            } catch (Exception e) {
                Log.e(TAG, "Error", e);
                return null;
            }

            if (uu.size() == 1) {
                Bundle args = uris.get(0);
                Uri u = uu.get(0);
                final String fn = args.getString("filename");
                final String type = args.getString("mimetype");

                intent.setDataAndType(u, type);
                intent.setAction(Intent.ACTION_SEND);
                intent.putExtra(Intent.EXTRA_EMAIL, new String[]{""});
                intent.putExtra(Intent.EXTRA_SUBJECT, fn);
                intent.putExtra(Intent.EXTRA_STREAM, u);

                FileProvider.grantPermissions(context, intent);
            }

            if (uu.size() > 1) {
                intent.setType("application/octet-stream");
                intent.setAction(Intent.ACTION_SEND_MULTIPLE);
                intent.putExtra(Intent.EXTRA_EMAIL, new String[]{""});
                intent.putExtra(Intent.EXTRA_SUBJECT, title);
                intent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uu);

                FileProvider.grantPermissions(context, intent);
            }

            return intent;
        }

        protected void onPostExecute(Intent intent) {
            if (intent == null)
                return;
            context.startActivity(Intent.createChooser(intent, null));
            enable();
        }
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        Activity a = getActivity();
        if (a instanceof DialogInterface.OnDismissListener)
            ((DialogInterface.OnDismissListener) a).onDismiss(dialog);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder b = new AlertDialog.Builder(getActivity())
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.dismiss();
                                onDismiss(dialog);
                            }
                        }
                )
                .setNeutralButton("Share All",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                            }
                        }
                )
                .setTitle("Share")
                .setView(createView(LayoutInflater.from(getContext()), null, savedInstanceState));
        d = b.create();
        d.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                d.getButton(DialogInterface.BUTTON_NEUTRAL).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        share();
                    }
                });
            }
        });
        return d;
    }

    @Nullable
    @Override
    public View getView() {
        return null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return null;
    }

    public static String getMimeType(String url) {
        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        if (extension != null) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }
        return type;
    }

    public View createView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.share_incoming, container, false);

        text = (TextView) v.findViewById(R.id.share_text);
        list = (LinearLayout) v.findViewById(R.id.list);
        View filepanel = v.findViewById(R.id.share_file_panel);

        title = getArguments().getString("text");
        Object uu = getArguments().get("uris");

        if (uu != null) {
            for (Bundle u : (ArrayList<Bundle>) uu) {
                add(u);
            }
        } else {
            filepanel.setVisibility(View.GONE);
        }

        if (title != null) {
            text.setText(title);
            getArguments().putString("text", title);
        }

        return v;
    }

    void add(final Bundle uri) {
        final String u = uri.getString("uri");
        final String fn = uri.getString("filename");

        LayoutInflater inflater = LayoutInflater.from(getContext());
        View v = inflater.inflate(R.layout.share_file, list, false);
        ProgressBar p = (ProgressBar) v.findViewById(R.id.share_progress);

        bars.put(u, p);

        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                disable();
                ArrayList<Bundle> uu = new ArrayList<>();
                uu.add(uri);
                new DownloadImageTask().execute(uu);
            }
        });

        TextView filename = (TextView) v.findViewById(R.id.share_filename);
        TextView filesize = (TextView) v.findViewById(R.id.share_filesize);

        filename.setText(fn);
        Long length = new Long(uri.get("filesize").toString());
        filesize.setText(SMSApplication.formatSize(getContext(), length));

        list.addView(v);
    }

    void share() {
        disable();
        title = text.getText().toString();
        getArguments().putString("text", title);
        for (String key : bars.keySet()) {
            ProgressBar p = bars.get(key);
            p.setProgress(0);
        }
        ArrayList<Bundle> uu = (ArrayList<Bundle>) getArguments().get("uris");
        new DownloadImageTask().execute(uu);
    }

    @Override
    public void onResume() {
        super.onResume();
        SMSApplication.shareClear(getActivity());
    }

    void disable() {
        d.getButton(DialogInterface.BUTTON_NEUTRAL).setEnabled(false);
        for (int i = 0; i < list.getChildCount(); i++) {
            list.getChildAt(i).setEnabled(false);
        }
    }

    void enable() {
        d.getButton(DialogInterface.BUTTON_NEUTRAL).setEnabled(true);
        for (int i = 0; i < list.getChildCount(); i++) {
            list.getChildAt(i).setEnabled(true);
        }
    }
}
