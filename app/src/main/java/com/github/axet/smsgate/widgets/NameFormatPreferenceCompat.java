package com.github.axet.smsgate.widgets;

import android.Manifest;
import android.content.Context;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.support.v7.preference.PreferenceScreen;
import android.util.AttributeSet;

import com.github.axet.smsgate.app.SMSApplication;
import com.github.axet.smsgate.app.Storage;
import com.github.axet.smsgate.providers.SIM;

import java.util.ArrayList;
import java.util.Arrays;

public class NameFormatPreferenceCompat extends com.github.axet.androidlibrary.preferences.NameFormatPreferenceCompat {
    public static Storage.SMSMessage DEMO = new Storage.SMSMessage(1493561080000l, "IN", "+1333445566", "Contact Name", "Message text", 1);
    public static String[] PP = new String[]{Manifest.permission.READ_PHONE_STATE};

    ArrayList<CharSequence> defValues;

    public static void onResume(PreferenceFragmentCompat prefs) {
        PreferenceScreen screen = prefs.getPreferenceScreen();
        for (int i = 0; i < screen.getPreferenceCount(); i++) {
            Preference pref = screen.getPreference(i);
            if (pref instanceof NameFormatPreferenceCompat)
                ((NameFormatPreferenceCompat) pref).onResume();
        }
    }

    public NameFormatPreferenceCompat(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public NameFormatPreferenceCompat(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public NameFormatPreferenceCompat(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public NameFormatPreferenceCompat(Context context) {
        super(context);
    }

    @Override
    public void create() {
        super.create();
        onCreate();
    }

    SIM getSIM() {
        SIM sim;
        if (Storage.permitted(getContext(), PP)) {
            sim = SMSApplication.from(getContext()).getSIM();
        } else {
            sim = new SIM() {
                @Override
                public int getCount() {
                    return 1;
                }
            };
        }
        return sim;
    }

    public void onCreate() {
        CharSequence[] vv = getEntryValues();
        defValues = new ArrayList<>(Arrays.asList(vv));
        onAutoFill(filterValues(vv));
    }

    public CharSequence[] filterValues(CharSequence[] vv) {
        SIM sim = getSIM();
        if (sim.getCount() < 2) { // filter %S if here is only one SIM card
            for (int i = 0; i < vv.length; i++) {
                String s = vv[i].toString();
                s = s.replaceAll(Storage.FORMAT_SIM, "");
                s = s.replaceAll(" +", " ");
                s = s.trim();
                vv[i] = s;
            }
        }
        return vv;
    }

    public void onResume() {
        CharSequence[] vv = defValues.toArray(new CharSequence[0]);
        onAutoFill(filterValues(vv));
        setEntryValues(vv);
    }

    @Override
    public String getFormatted(String str) {
        SIM sim = getSIM();
        return Storage.getNameFormatted(sim, str, DEMO);
    }
}
