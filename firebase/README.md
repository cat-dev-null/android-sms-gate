# FCM messaging


Firebase messaging has different json format from FCM REST.

```javascript
  var message = {
    data: {
      score: '850',
      time: '2:45'
    },
    topic: topic
  };
```

  * https://firebase.google.com/docs/cloud-messaging/admin/send-messages
